﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerivcoDGantHighCard
{
    public enum Suit 
    {
        diamonds = 0,
        spades = 1,
        clubs = 2,
        hearts = 3
    }
}
