﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DerivcoDGantHighCard
{
    public static class DeckBuilder
    {
        public static Queue<Card> CreateCards()
        {
            Queue<Card> cards = new Queue<Card>();
            //DG: 6) Now make the game work for a deck with 20 cards per suit
            foreach (Suit suit in Enum.GetValues(typeof(Suit)))
            {
                for (int i = 2; i <= 21; i++)
                {
                    cards.Enqueue(new Card()
                    {
                        Suit = suit,
                        Value = i,
                        DisplayName = GetShortDisplayName(i, suit)
                    });
                }
            }
            //DG: 5) Make one of the cards in the deck a wild card ( beats all others ).
            cards.Enqueue(new Card 
            { 
                DisplayName = "Wild Card",
                Suit = new Suit(),
                Value = 100
            });

            return ShuffleDeck(cards);
        }

        /// <summary>
        /// Returns a shuffled set of cards
        /// </summary>
        /// <param name="cards"></param>
        /// <returns></returns>
        private static Queue<Card> ShuffleDeck(Queue<Card> cards)
        {
            //Shuffle the Deck of Cards.
            List<Card> transformedCards = cards.ToList();
            Random r = new Random(DateTime.Now.Millisecond);
            for (int n = transformedCards.Count - 1; n > 0; --n)
            {
                //Step 2: Randomly pick a card which has not been shuffled.
                int k = r.Next(n + 1);

                //Step 3: Swap the selected item with the last "unselected" card in the collection.
                Card temp = transformedCards[n];
                transformedCards[n] = transformedCards[k];
                transformedCards[k] = temp;
            }

            Queue<Card> shuffledCards = new Queue<Card>();
            foreach (var card in transformedCards)
            {
                shuffledCards.Enqueue(card);
            }

            return shuffledCards;
        }

        private static string GetShortDisplayName(int value, Suit suit)
        {
            string displayValue = "";

            if (value >= 2 && value <= 10 || value >= 14 && value <= 20 )
            {
                displayValue = value.ToString();
            }
            else if (value == 11)
            {
                displayValue = "J";
            }
            else if (value == 12)
            {
                displayValue = "Q";
            }
            else if (value == 13)
            {
                displayValue = "K";
            }
            //DG: According the feature (6) We upgrade the value for the Ace, 
            // to continue being the highest value for each Suit. We assume this behaviour. 
            else if (value == 21)
            {
                displayValue = "A";
            }

            return (displayValue + Enum.GetName(typeof(Suit), suit)[0]).ToUpper();
        }
    }
}
