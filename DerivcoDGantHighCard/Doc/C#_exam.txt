Question 1
==========

Please take a look at the following code. Please fix, refactor and improve the code to the level that you would consider to be production standard.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Question1
{    
    class Program
    {
        static char[] transcode = new char[64];

        private static void prep()
        {                        
            for (int i = 0; i < 64; i++)  
            {    
                transcode[i] = (char)((int)'A' + i);                
                if (i > 25) transcode[i] = (char)((int)transcode[i] + 6 );
                if (i > 51) transcode[i] = (char)((int)transcode[i] - 0x4b);
            }
            transcode[62] = '+';
            transcode[63] = '/';            
            transcode[64] = '=';        
        }

        static void Main(string[] args) 
        {            
            prep();
  
            string test_string = "This is a test string";

            if ( Convert.ToBoolean( String.Compare( test_string, decode(encode(test_string))))) 
            {
                Console.WriteLine("Test succeeded");                         
            }
            Console.WriteLine("Test failed");                              
        }
       

        private static string encode(string input)
        {
            int l = input.Length;
            int cb = (l/3 + ( Convert.ToBoolean(l % 3) ? 1 : 0) ) * 4;   

            char[] output = new char[cb];
            for ( int i = 0; i < cb; i++ )
            {
                output[i] = '=';
            }
 
            int c = 0;
            int reflex = 0;
            const int s = 0x3f;             
 
            for (int j = 0; j < l; j++)
            {
                reflex <<= 8;
                reflex &= 0x00ffff00;         
                reflex += input[j];
 
                int x = ((j%3)+1)*2;          
                int mask = s << x;
                while (mask >= s)
                {
                    int pivot =  (reflex & mask) >> x;
                    output[c++] = transcode[pivot];
                    int invert = ~mask;
                    reflex &= invert;
                    mask >>= 6;
                    x -= 6;
                }
            }
 
            switch (l%3)
            {
                case 1:
                    reflex <<= 4;
                    output[c++] = transcode[reflex];   
                
                case 2:
                    reflex <<= 2;
                    output[c++] = transcode[reflex];
                    break;
                
            }
            Console.WriteLine("{0} --> {1}\n", input, new string( output) );
            return new string( output );
        }


        private static string decode(string input)
        {
            int l = input.Length;
            int cb = (l/4 + ((Convert.ToBoolean(l%4))?1:0))*3+1;   
            char[] output = new char[cb];        
            int c = 0;
            int bits = 0;
            int reflex = 0;
            for (int j = 0; j < l; j++)
            {
                reflex <<= 6;
                bits += 6;
                bool fTerminate = ('=' == input[j]);
                if (!fTerminate)
                    reflex += indexOf(input[j]);
    
                while (bits >= 8)
                {
                    int mask = 0x000000ff << (bits % 8);                                        
                    output[c++] = (reflex & mask) >> (bits % 8);    
                    int invert = ~mask;
                    reflex &= invert;
                    bits -= 8;
                }
 
                if (fTerminate)
                    break;
            }
            Console.WriteLine("{0} --> {1}\n", input, new string( output ));
            return new string( output );                    
        }

        private static int indexOf(char ch)
        {
            int index;
            for (index = 0; index < transcode.Length; index++)
            if (ch == transcode[index])
                break;    
            return index;
        }
    }
}

Question 2
==========
Below is an implementation for a game of HighCard, where two cards are drawn from a 52 card deck, and the highest card wins.

Please can you refactor this code to add in the ability to:

 1) Support ties when the face value of the cards are the same.
 2) Allow for the ties to be resolved as a win by giving the different suits precedence.
 3) Support for Multiple Decks. 
 4) Support the abilty to never end in a tie, by dealing a further card to each player.
 5) Make one of the cards in the deck a wild card ( beats all others ).
 6) Now make the game work for a deck with 20 cards per suit
 
Please apply all the best practices you would in what you consider to be "production ready code"

using System;
using System.Text;

namespace Question2
{
  class HighCard
  {
    public HighCard()
    {
    }

    public bool Play()
    {
      Random rnd = new Random();

      int i = rnd.Next() % 52 + 1;
      int j = rnd.Next() % 52 + 1;

      return i < j;
    }
  }
  
  class Program
  {
    static void Main(string[] args)
    {
      HighCard card = new HighCard();
      if (card.Play())
      {
        Console.WriteLine("win");
      }
      else
      {
        Console.WriteLine("lose");
      }
    }
  }
}

Question 3
==========

Please can you give a written prose on your design choices in question 2

For this implementation I have created the next classes: 

Class Player - Single player

Class Card - single card

// 2) Allow for the ties to be resolved as a win by giving the different suits precedence.
Enum Suit - With pre assigned Precedence to solve the Ties.

Class DeckBuilder - To manage creation of a Deck of Cards, and Shuffle it..

-------------------------------------------------------------------
//DG: 6) Now make the game work for a deck with 20 cards per suit
-------------------------------------------------------------------
I have created a DeckBuilder Class to manage the Deck creation:

Using a Queue<Card> to handle the Deck:
            
			Queue<Card> cards = new Queue<Card>(); 
			
In the last feature, we create 20 Cards per Suit. 

            foreach (Suit suit in Enum.GetValues(typeof(Suit)))
            {
                for (int i = 2; i <= 21; i++) .......  
			}
			
//DG: 5) Make one of the cards in the deck a wild card ( beats all others ):
Onces Deck is created I add a new Card to the Queue as Wild Card with value 100.

            cards.Enqueue(new Card 
            { 
                DisplayName = "Wild Card",
                Suit = new Suit(),
                Value = 100
            });			

//DG: According the feature (6) We upgrade the value for the Ace, 
// to continue being the highest value for each Suit. We assume this behaviour. 
I kept the same values for the rest of figures. 

//DG: I created a ShuffleDeck Method to return the Deck of Cards Shuffled.

        private static Queue<Card> ShuffleDeck(Queue<Card> cards)
