﻿//Question 2
//==========
//Below is an implementation for a game of HighCard, where two cards are drawn from a 52 card deck, and the highest card wins.

//Please can you refactor this code to add in the ability to:

// 1) Support ties when the face value of the cards are the same.
// 2) Allow for the ties to be resolved as a win by giving the different suits precedence.
// 3) Support for Multiple Decks.
// 4) Support the abilty to never end in a tie, by dealing a further card to each player.
// 5) Make one of the cards in the deck a wild card ( beats all others ).
// 6) Now make the game work for a deck with 20 cards per suit

//Please apply all the best practices you would in what you consider to be "production ready code"



using System;
using System.Collections.Generic;

namespace DerivcoDGantHighCard
{

    public class HighCard
    {
        #region Properties
        public Queue<Card> Deck { get; set; }
        public Player Player1;
        public Player Player2;

        public int NumberOfDecks { get; set; }
        public int Gameresult { get; set; }

        public bool suitsPrecedence = false;
        public ConsoleKeyInfo option2;

        #endregion

        public HighCard()
        {
        }


        #region HelperMethods

        public void Play(string player1name, string player2name)
        {
            Player1 = new Player(player1name);
            Player2 = new Player(player2name);
            Deck = DeckBuilder.CreateCards();

            //DG:  4) Support the abilty to never end in a tie,
            //      by dealing a further card to each player.
            do
            {
                DealCardsTwoPlayers();

                Console.WriteLine("\n" + Player1.Name + " plays "
                                  + Player1.Card.DisplayName + ", "
                                  + Player2.Name + " plays " + Player2.Card.DisplayName);
                Gameresult = CompareCards();

            } while (Gameresult == 0);


            if (Gameresult == 1)
            {
                Console.WriteLine("\n" + Player1.Name + " WIN! "
                                  + Player1.Card.DisplayName);
                Console.ReadKey();
            }
            else if (Gameresult == 2)
            {
                Console.WriteLine("\n" + Player2.Name + " WIN! "
                                  + Player2.Card.DisplayName);
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("Tie");
                Console.ReadKey();
            }
        }

        public void DealCardsTwoPlayers()
        {
            Player1.Card = Deck.Dequeue();
            Player2.Card = Deck.Dequeue();

            Player1.Deck.Enqueue(Player1.Card);
            Player2.Deck.Enqueue(Player2.Card);
        }

        /// <summary>
        /// Compare the the Highest value Card from the Two players,
        /// and returns the result value.
        /// </summary>
        /// <returns> Integer: The GameResult Value </returns>
        public int CompareCards()
        {
            if (Player1.Card.Value > Player2.Card.Value) return 1;
            if (Player1.Card.Value < Player2.Card.Value) return 2;

            if (Player1.Card.Value == Player2.Card.Value)
            {
                if (suitsPrecedence) //DG: 2) Allow for the ties to be resolved as a win by giving the different suits precedence.
                {
                    if (Player1.Card.Suit > Player2.Card.Suit) return 1;
                    if (Player1.Card.Suit < Player2.Card.Suit) return 2;
                }
                else
                {
                    Console.WriteLine("\n There is a TIE!! ");
                    return 0;
                }
            }
            return -1;
        }

        internal void ShowMenu()
        {
            List<char> charOptions = new List<char>();
            charOptions.Add('Y'); charOptions.Add('y');
            charOptions.Add('N'); charOptions.Add('n');

            do
            {
                Console.Clear();
                Console.WriteLine("\n2) Allow for the ties to be resolved as a win by giving the different suits precedence? (Y/N)");
                option2 = Console.ReadKey();

                Console.WriteLine("\n3) Please enter the number of Decks: ");
                var userInput = Console.ReadKey(); // get user input

                // We should check char for a Digit, so that we will not get exceptions from Parse method
                if (char.IsDigit(userInput.KeyChar))
                {
                    NumberOfDecks = int.Parse(userInput.KeyChar.ToString());
                    Console.WriteLine("\nNumber Of Decks : {0}", NumberOfDecks);
                }
                else
                {
                    NumberOfDecks = -1;  // Else we assign a default value
                    Console.WriteLine("\nUser didn't insert a Number");
                    Console.ReadKey();
                }

            }while (!charOptions.Contains(option2.KeyChar) || !(NumberOfDecks > 0)) ;

            if (option2.KeyChar == 'y' || option2.KeyChar == 'Y') this.suitsPrecedence = true;
            else this.suitsPrecedence = false;
        }

        #endregion
    }



    class Program
    {
        static void Main(string[] args)
        {
            HighCard game = new HighCard();
            game.ShowMenu();

            for (int i = 0; i < game.NumberOfDecks; i++)
            {
                Console.WriteLine("\n ***********************************************************");
                Console.WriteLine("\n                   PLAYING WITH DECK: " + i);
                Console.WriteLine("\n ***********************************************************");
                game.Play("Daniel Gant", "Derivco");
            }

            Console.ReadKey();
        }
    }
}
