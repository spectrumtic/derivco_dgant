﻿using System.Collections.Generic;

namespace DerivcoDGantHighCard
{
    public class Player
    {
        public Player(string name)
        {
            Name = name;
            Card = new Card();
            Deck = new Queue<Card>();
        }

        public string Name { get; set; }
        public Card Card { get; set; }
        public Queue<Card> Deck { get; set; }
    }
}