﻿using System;
using DerivcoDGantHighCard;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace DerivcoUnitTestProject
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void PlayGame()
        {

            //Arrange
            HighCard game = new HighCard();
            game.Player1 = new Player("Daniel Gant");
            game.Player2 = new Player("Derivco");
            game.Deck = DeckBuilder.CreateCards();
            game.DealCardsTwoPlayers();

            //ACT
            game.Gameresult = game.CompareCards();

            //ASSERT
            Assert.IsNotNull(game.Gameresult);
        }

        [TestMethod]
        public void PlayGameTie()
        {
            //Arrange
            HighCard game = new HighCard();
            game.Player1 = new Player("Daniel Gant");
            game.Player2 = new Player("Derivco");
            game.Deck = DeckBuilder.CreateCards();
            game.suitsPrecedence = false;
            game.Player1.Card.Value = 10;
            game.Player2.Card.Value = 10;

            //ACT
            game.Gameresult = game.CompareCards();

            //ASSERT
            Assert.IsNotNull(game.Gameresult);
            Assert.AreEqual(game.Gameresult, 0);
        }

        [TestMethod]
        public void PlayGameSuitsPrecedencePlayer1Wins()
        {
            //Arrange
            HighCard game = new HighCard();
            game.Player1 = new Player("Daniel Gant");
            game.Player2 = new Player("Derivco");
            game.suitsPrecedence = true;
            game.Player1.Card.Value = 10;
            game.Player2.Card.Value = 10;
            game.Player1.Card.Suit = Suit.clubs;
            game.Player2.Card.Suit = Suit.diamonds;

            //ACT
            game.Gameresult = game.CompareCards();

            //ASSERT
            Assert.IsNotNull(game.Gameresult);
            Assert.AreEqual(game.Gameresult, 1);
        }

        [TestMethod]
        public void PlayGameNoSuitsPrecedenceTIE()
        {
            //Arrange
            HighCard game = new HighCard();
            game.Player1 = new Player("Daniel Gant");
            game.Player2 = new Player("Derivco");
            game.suitsPrecedence = false;
            game.Player1.Card.Value = 10;
            game.Player2.Card.Value = 10;
            game.Player1.Card.Suit = Suit.clubs;
            game.Player2.Card.Suit = Suit.diamonds;

            //ACT
            game.Gameresult = game.CompareCards();

            //ASSERT
            Assert.IsNotNull(game.Gameresult);
            Assert.AreEqual(game.Gameresult, 0);
        }


        [TestMethod]
        public void PlayGameSuitsPrecedencePlayer2Wins()
        {
            //Arrange
            HighCard game = new HighCard();
            game.Player1 = new Player("Daniel Gant");
            game.Player2 = new Player("Derivco");
            game.suitsPrecedence = true;
            game.Player1.Card.Value = 10;
            game.Player2.Card.Value = 10;
            game.Player1.Card.Suit = Suit.diamonds;
            game.Player2.Card.Suit = Suit.clubs;

            //ACT
            game.Gameresult = game.CompareCards();

            //ASSERT
            Assert.IsNotNull(game.Gameresult);
            Assert.AreEqual(game.Gameresult, 2);
        }

        [TestMethod]
        public void PlayGameWildCardPlayer1Wins()
        {
            //Arrange
            HighCard game = new HighCard();
            game.Player1 = new Player("Daniel Gant");
            game.Player2 = new Player("Derivco");
            game.suitsPrecedence = true;
            game.Player2.Card.Value = 10;
            game.Player2.Card.Suit = Suit.diamonds;

            //ACT
            game.Deck = DeckBuilder.CreateCards();
            var wildCard = game.Deck.ToList().Where(card => card.Value == 100).FirstOrDefault();
            game.Player1.Card = wildCard;

            game.Gameresult = game.CompareCards();

            //ASSERT
            Assert.IsNotNull(game.Gameresult);
            Assert.AreEqual(game.Gameresult, 1);
        }


    }
}
