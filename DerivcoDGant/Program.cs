﻿//================
//  Question 1  //
//================

//Please take a look at the following code. Please fix, refactor and improve 
// the code to the level that you would consider to be production standard.

using System;

namespace DerivcoDGantCodeDecode
{
    class Program
    {
        static char[] transcode = new char[65];
        private static void prep()
        {
            for (int i = 0; i < 64; i++)
            {
                transcode[i] = (char)((int)'A' + i);
                if (i > 25) transcode[i] = (char)((int)transcode[i] + 6);
                if (i > 51) transcode[i] = (char)((int)transcode[i] - 0x4b);
            }
            transcode[62] = '+';
            transcode[63] = '/';
            transcode[64] = '=';
        }
        static void Main(string[] args)
        {
            prep();

            string test_string = "This is a test string";

            var encodeOutput = Program.encode(test_string);
            var decodeOutput = decode(encodeOutput);

            var root = test_string;
            var root2 = decodeOutput;

            bool result = root.Equals(root2, StringComparison.Ordinal);
            bool areEqual = String.Equals(root, root2, StringComparison.Ordinal);
            int comparisonResult = String.Compare(root, root2, comparisonType: StringComparison.Ordinal);

            Console.WriteLine($"\nOrdinal ignore case: <{root}> and <{root2}> are {(result ? "equal." : "not equal.")}");
            Console.WriteLine($"\nOrdinal static ignore case: <{root}> and <{root2}> are {(areEqual ? "equal." : "not equal.")}");
            if (comparisonResult < 0)
                Console.WriteLine($"\n<{root}> is less than <{root2}>");
            else if (comparisonResult > 0)
                Console.WriteLine($"\n<{root}> is greater than <{root2}>");
            else
                Console.WriteLine($"\n<{root}> and <{root2}> are equivalent in order\n");


            if (!Convert.ToBoolean(comparisonResult))
            {
                Console.WriteLine("Test succeeded");
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("Test failed");
                Console.ReadKey();
            }
        }


        private static string encode(string input)
        {
            int l = input.Length;
            int cb = (l / 3 + (Convert.ToBoolean(l % 3) ? 1 : 0)) * 4;

            char[] output = new char[cb];
            for (int i = 0; i < cb; i++)
            {
                output[i] = '=';
            }

            int c = 0;
            int reflex = 0;
            const int s = 0x3f;

            for (int j = 0; j < l; j++)
            {
                reflex <<= 8;
                reflex &= 0x00ffff00;
                reflex += input[j];

                int x = ((j % 3) + 1) * 2;
                int mask = s << x;
                while (mask >= s)
                {
                    int pivot = (reflex & mask) >> x;
                    output[c++] = transcode[pivot];
                    int invert = ~mask;
                    reflex &= invert;
                    mask >>= 6;
                    x -= 6;
                }
            }

            switch (l % 3)
            {
                case 1:
                    reflex <<= 4;
                    output[c++] = transcode[reflex];
                    break;

                case 2:
                    reflex <<= 2;
                    output[c++] = transcode[reflex];
                    break;

            }
            Console.WriteLine("{0} --> {1}\n", input, new string(output));
            return new string(output);
        }


        private static string decode(string input)
        {
            int l = input.Length;
            int cb = (l / 4 + ((Convert.ToBoolean(l % 4)) ? 1 : 0)) * 3;
            char[] output = new char[cb];
            int c = 0;
            int bits = 0;
            int reflex = 0;
            for (int j = 0; j < l; j++)
            {
                reflex <<= 6;
                bits += 6;
                bool fTerminate = ('=' == input[j]);
                if (!fTerminate)
                    reflex += indexOf(input[j]);

                while (bits >= 8)
                {
                    int mask = 0x000000ff << (bits % 8);
                    output[c++] = (char)((reflex & mask) >> (bits % 8));
                    int invert = ~mask;
                    reflex &= invert;
                    bits -= 8;
                }

                if (fTerminate)
                    break;
            }
            Console.WriteLine("{0} --> {1}\n", input, new string(output));
            return new string(output);
        }

        private static int indexOf(char ch)
        {
            int index;
            for (index = 0; index < transcode.Length; index++)
                if (ch == transcode[index])
                    break;
            return index;
        }
    }
}